# OpenConnect GUI

This is the development space of OpenConnect VPN graphical client (GUI).
See the [OpenConnect VPN GUI web site](https://gui.openconnect-vpn.net/)
for detailed description, screen shots and other related projects.

## Supported Platforms
- Microsoft Windows 7 and newer
- macOS 10.12 and newer

## Development info
- [Software requirements](docs/sw_requirements.md)
- [Development with QtCreator](docs/dev_QtCreator.md)
- [Command line compilation](docs/dev_commandLine.md)

## Other
- [Creating release package](docs/release.md)
- [OpenConnect library compilation and dependencies](docs/openconnect.md)
- [Web page maintenance](https://gitlab.com/openconnect/openconnect-gui-web)
- [Snapshot builds](docs/snapshots.md)
- [AppVeyor CI builds](https://ci.appveyor.com/project/nmav/openconnect-gui/history)

# License
The content of this project itself is licensed under the [GNU General Public License v2](LICENSE.txt)
